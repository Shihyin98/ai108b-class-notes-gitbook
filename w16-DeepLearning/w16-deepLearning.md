# Week 16 - 深度學習

> Date: 2020/ 06/ 18 (Thur.)

## 課堂大綱 (Outline)
* 範例講解
  * [ConvNetJS MNIST《手寫數字辨識》](https://cs.stanford.edu/people/karpathy/convnetjs/demo/mnist.html)
  * [ConvNetJS CIFAR-10《影像物體辨識》](https://cs.stanford.edu/people/karpathy/convnetjs/demo/cifar10.html)

<br>

* Models
  * CNN (卷積神經網路)
  * RNN (循環神經網路)
  * GAN (生成對抗網絡)

--

# 上課內容 (Course Content)

# Memo
* What is Deep Learning?
* 深度學習就是神經網路，有許多不同種類的模型。
* ex: CNN、RNN、GAN、BART... [[1]](-)[[2]](-)[[3]](-)
* NodeJS雖然在深度學習上遠不如Python，但它在網頁的示範上有一些好處。 ex: ConvNetJS MNIST
* [解密深度學習](https://brohrer.mcknote.com/zh-Hant/how_machine_learning_works/deep_learning_demystified.html)
* Google 透過 **強化式學習** 打敗世界棋王。
* Reinforcement Learning (強化學習)
* [老師的課外補充教材](https://github.com/cccbook/aijs/wiki?fbclid=IwAR3Wq9yhE2nO2voapBiYFElMnfhqoMV2Pp-FnxFTgsgwDZ2EOJok2ma4teU)
* [深度學習教科書](https://github.com/exacity/deeplearningbook-chinese/releases/download/v0.5-beta/dlbook_cn_v0.5-beta.pdf?fbclid=IwAR1-sgQOPkYvNNGjafj2BFPRMCoKpiheWEZL81aR0gNSSeu4izkilU5Q-Fo)
* 真正最好的預訓練模型都已經在商業中使用了，我們不須自己做，**善用線上現成的模型或套件，透過 API進行接口對對接**。
  * [googletrans 3.0.0_PyPI](https://pypi.org/project/googletrans/)
  * [deepl 0.3_PyPI](https://pypi.org/project/deepl/)
* **運用現成的線上翻譯**，有兩中方法：
  * 使用 API，進行呼叫。
  * 利用 headless broswer(無頭瀏覽器)，進行直接開啟瀏覽器，線上翻譯後，再取回。
<br>

* [NodeJS的深度學習資源](https://cs.stanford.edu/people/karpathy/convnetjs/?fbclid=IwAR3N3b54Vk3TIVx4Uv-Sm3NoogeG6OjvA9lwDl89DxA6sd-DDRMyNLYOsqs)：
  * ConvNetJS: CNN 卷積神經網路
  * Neataptic: LSTM 長短期記憶網路
* 老師寫的 Deep Learning with NodeJS 課件範例： [AI6 -- Artificial Intelligence Library (based on j6)](https://github.com/ccckmit/ai6)
* 深度學習技術專家之參考資料：[Andrej Karpathy blog](http://karpathy.github.io/)

<br>

* Attention Model (注意力模型) 
  * [Seq2seq pay Attention to Self Attention: Part 1(中文版)](https://medium.com/@bgg/seq2seq-pay-attention-to-self-attention-part-1-中文版-2714bbd92727)
  * [Attention Mechanism](https://blog.floydhub.com/attention-mechanism/)
  * [Making Transformer networks simpler and more efficient](https://ai.facebook.com/blog/making-transformer-networks-simpler-and-more-efficient/)
  * [一文讀懂Attention：Facebook曾拿CNN秒殺谷歌，現如今谷歌拿它秒殺所有人](https://kknews.cc/zh-tw/tech/e8e4poz.html)

<br>


* 翻譯
  * [進擊的 BERT：NLP 界的巨人之力與遷移學習_李宏毅](https://leemeng.tw/attack_on_bert_transfer_learning_in_nlp.html?fbclid=IwAR2tqBw5Qun37LAL0MNomrAqDK7tnYrfIHABGEngC_WKHndaac05Nzy-mLA)
  * [Google翻譯](https://translate.google.com.tw/?fbclid=IwAR1MnG1fbSYmRjZflW1wX5jWKzGHJKnaqVt5myDKoOPTXPMK06lJi-Kkglk)
  * [腾讯翻译君](https://fanyi.qq.com/)
  * [DeepL](https://www.deepl.com/translator?fbclid=IwAR2LkpA5Jnmk3L4d7FaYp-akhnCzNd0NZJC10Up3kuKsWzr3d2bdbk9iudk)
  * 翻譯可能存在的問題：英文的專有名詞，不必特別英翻中，英翻英即可。
  * 英文翻中文，大陸的軟件相對成熟。

<br>

--

# ConvNetJS MNIST《手寫數字辨識》
![mnist](img-models/mnist.png)
* MNIST 深度學習基礎入門例子
* 訓練時的　Loss 損失、錯誤率、錯誤評估值
* **Loss　降得越低，Accuracy(準確率)越高。**
<br>

|![cnn-demo-mnist-loss](img-models/cnn-demo-mnist-loss.png)|
|:--:|
|[ConvNetJS MNIST](https://cs.stanford.edu/people/karpathy/convnetjs/demo/mnist.html)|

<br>

* 辨識圖片是 **手寫數字**　照片只有　**黑、白** 組成，所以只需要用到 **1 byte** (pixel)。
* 圖像值：0-255
* 如果是彩色圖片，則需要 **3 bytes** 以上，因為 **R、G、B**。 [[4]](-)
* CNN 的主要功能層：conv, relu, pool(三者輪流) + fc + softmax函數(得結果)

--

# CNN (卷積神經網路)
* Slide: [深度學習的捲積神經網路--(使用JavaScript/node.js實作)](https://www.slideshare.net/ccckmit/javascript-nodejs?fbclid=IwAR217on9iAxTKsq5QtzkujQlqBLJTOPH7eBX8S5o7LjVLp615_TTptYvqpY)
* Slide-Reference: [Hacker's guide to Neural Networks](http://karpathy.github.io/neuralnets/)

<br>

* 功能層之 conv, relu, pool...到之後的殘差網路等，其目的就是為了**提取圖片的特徵值**，以便來做模型的樸片識別之依據。
* 透過設計幾層 conv 或relu 或pool等，來提升模型的辨識率。 (當然電腦的算力也要足夠)
<br>


|![javascript-nodejs-35-cnn](img-models/javascript-nodejs-35-cnn.jpg)|
| :---:|
|  slide-page-35  |

<br>

* 初級特徵： 由一些不同的直線、橫線、斜線，以及不同的顏色所組成。
<br>


|![javascript-nodejs-36-cnn](img-models/javascript-nodejs-36-cnn.jpg)|
| :---:|
|  slide-page-36  |

<br>

* 接著去卷積綜合出更上層的特徵
* 經過 Filter的遮罩，把掃到的區塊之內容(取樣)捲出來，每捲一次得一個值。
* **傳統的神經網路取樣數量小，造成模型訓練的問題，因為特徵太多。**
* 卷積神經網路，取樣數量大，也可以照樣幾乎得整張照片的樣本數，克服傳統神經網路的問題。
<br>

|![javascript-nodejs-37-cnn](img-models/javascript-nodejs-37-cnn.jpg)|
| :--: |
|  slide-page-37  |
<br>

* 也有結合反傳遞的演算法
<br>

|![javascript-nodejs-38-cnn](img-models/javascript-nodejs-38-cnn.jpg)|
| :--: |
|  slide-page-38  |
<br>


--

# ConvNetJS Cifar-10《影像物體辨識》
![cifar10](img-models/cifar10.jpg)
* Cifar-10 為 10種物體的分類
* 辨識圖片是 **10分類**　照片，因為是 **全彩** 組成，所以只需要用到 **3 byte** (RGB)。
* softmax()函數　主要是為了要判斷 10個結果的哪一個。
* 老師寫的課件範例 [CNN for Cifar-10 (修改版)](https://github.com/ccckmit/ai6/tree/master/book/nn/convnetjs)

--

# RNN (循環神經網路)
* Slide: [深度學習的RNN/LSTM循環神經網路 (使用node.js的neataptic套件實作)](https://www.slideshare.net/ccckmit/rnn-lstm-77568016?fbclid=IwAR1egK6QdEN_NpSMV3C_NonbTJBPmKQJ6lrHI7A-dtej5OmwhkkdtPsyPbI)
* Slide-Reference: [The Unreasonable Effectiveness of Recurrent Neural Networks](http://karpathy.github.io/2015/05/21/rnn-effectiveness/)

<br>

* 好處：**記憶歷史資料**，並透過歷史資料，**預測未來**。
* 缺點：**沒有長期記憶**
* LSTM (Long-Sort Term Memory) 長短期記憶網路
  * 改善 RNN的短期記憶
  * 目的：保住長期記憶
  * 可以將強的資訊進入長期記憶留存，弱的資訊則可以被遺忘。

<br>

* 舉例：數位邏輯 -- 《組合邏輯電路》和《循序邏輯電路》
* 《組合邏輯電路》
  * **非循環，輸出完全由輸入決定。**
  * 輸入如果相同，輸出也會相同。
  * 沒有任何記憶元件，**無法暫存數據**。
<br>

|![rnnlstm-nodejsneataptic-21-rnn](img-models/rnnlstm-nodejsneataptic-21-rnn.jpg)|
|:--:|
|   slide-page-21 |

<br>

* 《循序邏輯電路》
  * **循環，輸出由輸入、目前狀態決定。**
  * **正反器**為典型電路
  * 可以把訊號存於電路中，造成**記憶效應**。
<br>

|![rnnlstm-nodejsneataptic-27-rnn](img-models/rnnlstm-nodejsneataptic-27-rnn.jpg)|
|:--:|
|   slide-page-27 |

<br>

* 暫存器(Register)是由許多正反器組合而成的，以此類推記憶體(Memory)。
<br>

|![rnnlstm-nodejsneataptic-29-rnn](img-models/rnnlstm-nodejsneataptic-29-rnn.jpg)|
|:--:|
|   slide-page-29 |

<br>

* RNN 舉例：輸入一串文字時，就正確預測該字串每個字母的下一個字母會是什麼。 [[5]](-)
* 第一次輸入「h, e, l, l, o」，經過模型訓練後，之後如果再輸入「hello」，打到「h, e, l, l」，模型會**自動預測推測下一個**是「o」，並輸出。
<br>

![rnn-example-hello](img-models/rnn-example-hello.jpeg)

<br>

* Algorithm: BPTT (Backpropagation through time)
  * RNN 需要反傳遞的學習演算法


--


# GAN (生成對抗網絡)
* GAN, Generative Adversarial Network, 生成對抗網絡
* 非監督式學習
* 通過讓兩個神經網路 **相互博弈** 的方式進行學習

--


# 課上範例示範
* 執行範例程式碼： 11-deepLearning
* 範例筆記： [11-deepLearning/README.md]()

* 相關連結
  * [Torchvision](https://pytorch.org/docs/stable/torchvision/index.html?fbclid=IwAR2SMqYLV2cmtnF4EOyPySaem7v9jAGZZnSRAN0nZLYITVVqugIsPpmVQ0Y)
  * [Torchtext](https://pytorch.org/text/?fbclid=IwAR3CkoShdQYN94c3OKo1dLu_69gXx8EShWYjJQdy4DmWiUhd5zYUOgH0L_k)



## 課上答疑 (Q&A)
1.  Q: 圖像的卷積辨識的值，算是byte值嗎? 或是在訓練的時候，是取甚麼值來卷積，以供應之後做偵測的時候。
    A: 看輸入是黑白還彩色，黑白是 byte 矩陣彩色是三個 byte 矩陣。
    A: 神經網路的權重和輸出通常是浮點數矩陣或張量（高維矩陣）
    * Input images 如果是黑白的為 1byte的 Matrix；如果是彩色的為 3byte的 Matrix。


--


## Reference 
[1] [The Unreasonable Effectiveness of Recurrent Neural Networks](http://karpathy.github.io/2015/05/21/rnn-effectiveness/?fbclid=IwAR0TWM9hz5nZcAb7PcK7yRozi5eFV8XNNTCzM5mxnnz4kxqx60im641tYyc)<br>
[2] [生成對抗網絡 GAN_wiki](https://zh.wikipedia.org/wiki/生成对抗网络?fbclid=IwAR1St8PBHvqm4MksZO6ENbCCjs-G3ZKL6OBTYOJ-Rr60Ii2Gr73OqOaO3Ok)<br>
[3] [生成对抗网络(GAN)_莫烦PYTHON](https://morvanzhou.github.io/tutorials/machine-learning/torch/4-06-GAN/)<br>
[4] [影像處理基本概念](http://yuan.yocjh.kh.edu.tw/photoimpact/01.htm)<br>
[5] [速記AI課程－深度學習入門（二）](https://medium.com/@baubibi/速記ai課程-深度學習入門-二-954b0e473d7f)<br>



