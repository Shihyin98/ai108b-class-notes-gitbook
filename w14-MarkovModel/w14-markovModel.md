# Week 14 - 機器學習

> Date: 2020/ 06/ 04 (Thur.)

## 課堂大綱 (Outline)
* 範例講解
  * [EM 演算法實作](https://gitlab.com/ccckmit/ai2/-/blob/master/python/10-machineLearning/em/em.md?fbclid=IwAR0wRffIAbFYCW6nNwZK0zh5VdZUpbKpzVT8npMVa3mvUzp8AlwyjmEWur0)

<br>

* 隱馬可夫模型
* 維特比演算法
* 遺傳演算法
<br>

--

# 上課內容 (Course Content)

# Memo
* 動態規劃
<br>



# Viterbi Algorithm (維特比演算法)
* 一種 **動態規劃** 演算法
* 維特比演算法，是用於馬可夫模型上。

* [維特比演算法_維基百科](https://zh.wikipedia.org/wiki/维特比算法)
* [維特比演算法(Viterbi algorithm)_陳鍾誠](https://misavo.com/blog/陳鍾誠/書籍/人工智慧/10-機器學習/B3a-維特比算法?fbclid=IwAR0qfV2l-uVCPRqESdbA-qsoMcOm7oWDttWr9itCxgoCJ0mTqJNvLbN88q8)
<br>

* What is Dynamic Programming (動態規劃) ?
  * [動態規劃_陳鍾誠](https://gitlab.com/ccckmit/course/-/wikis/陳鍾誠/書籍/演算法/05-dynamicProgramming)
  * 透過**表格**的方式，**逐步由前面的結果推算出後面結果**的方法。
  * [排列組合](http://gadget.chienwen.net/x/math/percomb?fbclid=IwAR3LDEUGjjmZv90eNiQ4VrLqXi4pgQ8_9_LaFEnCSzDGWFwFqdXrp2ienwg)
  * 課堂範例： [CnkDynamic.js](se/algorithm/05-dynamicProgramming/combinatorial/CnkDynamic.js) + [巴斯卡三角形](https://zh.wikipedia.org/zh-tw/杨辉三角形?fbclid=IwAR0G3gptAO_moxRtul6dKVTVy93WtE_TdaY4AHpvWxkEkvjmIupciy4MGFU)
<br>

--

# HMM (隱藏式馬可夫模型)
* HMM, Hidden Markov Model
* [馬可夫鏈_陳鍾誠](https://gitlab.com/ccckmit/course/-/wikis/陳鍾誠/書籍/人工智慧/10-機器學習/B2-馬可夫鏈)
  * 轉移機率，並不會因為時間改變，而改變。
  * 有s0、s1 兩個狀態，其系統中 **出去的總和必須為「1」**，才符合機率。
    * 第一組是狀態本身的機率 P(s0)、P(s1)。
    * 第二組是狀態轉移的機率 Q(s0→s0)、Q(s0→s1)、Q(s1→s0)、Q(s1→s1)。
<br>

|![markov1](img/markov1.jpg)|
|:--:|
|馬可夫鏈|

<br>

* [隱藏式馬可夫模型_維基百科](https://zh.wikipedia.org/wiki/隐马尔可夫模型)
* 馬可夫過程
  * 定義： 每一個機率，轉移至另一個狀態時，**機率會固定**，不會隨著時間而改變。
  * [馬可夫過程_維基百科](https://zh.wikipedia.org/wiki/馬可夫過程)
<br>

|![markov-process](img/markov2.jpg)|
|:--:|
|馬可夫鏈|
<br>

* 我們可以觀察到 y的輸出結果，想逆推 x的狀態轉換，最可能狀態轉換的機率。
* 隱條件 x、顯狀態 y
* 隱條件 x，有好幾種狀態組合，最簡單使用 **暴力破解的方法**。
* 因此，我們引用 **維特比演算法**，**動態規劃**。
<br>

|![markov-process](img/markov_process.png)|
|:--:|
|馬可夫鏈|
<br>



# 遺傳演算法
* GA, genetic algorithm
* [遺傳演算法_wiki](https://zh.wikipedia.org/wiki/遗传算法)
* [實作：遺傳演算法_陳鍾誠](https://misavo.com/blog/陳鍾誠/書籍/人工智慧/02-爬山演算法/E-實作：遺傳演算法?fbclid=IwAR0J-5OmtDYOt61TpXIxNUEz9CxF_1ySHRAkmt57-Rxyv_og0VxSlJYuRU8)
* 模仿兩性生殖的演化機制，使用 **交配、突變等機制**，不斷改進群體適應的一種演算法。
* 在 **最佳化問題** 上，遺傳演算法是 **經常使用** 的方法之一。






--

# 課上範例示範
* 執行範例程式碼： [10-machineLearning_w15/ em/ em.py]()
* 範例筆記： [10-machineLearning_w15/README.md]()





## 課上答疑 (Q&A)



--


## Reference 




