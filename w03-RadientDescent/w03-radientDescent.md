# Week 03 - Radient Descent

> Date: 2020/ 03/ 19 (Thur.)

## 課堂大綱 (Outline)

* Gradient Descent (梯度下降法)
<br>

* 範例講解
  * 檔案路徑： [/python/03-neuralnet](https://gitlab.com/ccckmit/ai2/-/tree/master/python/03-neuralnet)
  * 範例檔案
    * 01-diff
      * diff.py
      * diff2.py
    * 02-gradient
      * vecGradient.py
      * npGradient.py
<br>


--

# 上課內容 (Course Content)

# Memo
* 課本： [人工智慧/03-神經網路/B-梯度下降法_陳鍾誠_GitLab](https://gitlab.com/ccckmit/course/-/wikis/陳鍾誠/書籍/人工智慧/03-神經網路/B-梯度下降法)
<br>



# Gradient Descent (梯度下降法)
* **梯度**就是**斜率最大的方向**，故梯度下降法，其實就是**朝著斜率最大的方向走**。
* 朝著《斜率最大》方向的《**正梯度**》走，那麼就會**愈走愈高**，但是如果朝著《**逆梯度**》方向走，那麼就會**愈走愈低**。
* 梯度下降法，就是**朝著《逆梯度》的方向走**，不斷下降，**直到到達梯度為 0 的點 (斜率最大的方向仍然是斜率為零)**，此時就已經到了一個《谷底》，也就是**區域最低點**。
* 缺點
  * 如果傾斜度大，步長大，容易動盪。
  * 如果傾斜度小，步長小，尋找的時間長。
<br>

* 《**反傳遞演算法**》，就是**用來加速《梯度計算》的一種方法**，這種方法依靠的是《**自動微分**》功能，想辦法從後面一層的差值，計算出前面一層應該調整的方向與大小。
<br>


--


# 課上範例示範筆記
* 執行範例程式碼： [03-neuralnet]()
* 範例筆記： [03-neuralnet/README.md]()
  * 01-diff
    * diff.py
    * diff2.py
  * 02-gradient
    * vecGradient.py
    * npGradient.py




## 課上答疑 (Q&A)


--


## Reference 




