# Week 02 - Hill Climbing

> Date: 2020/ 03/ 12 (Thur.)

## 課堂大綱 (Outline)

* Hill Climbing Algorithm (爬山演算法)
* Gradient Descent (梯度下降法)
<br>

* 範例講解
  * 檔案路徑： [/python/02-optimize](https://gitlab.com/ccckmit/ai2/-/tree/master/python/02-optimize)
  * 範例檔案
    * hillClimbing1.py
    * hillClimbing2.py
    * hillClimbing2r.py
    * hillClimbingNumber.py
    * hillClimbingArray.py
    * solutionArray.py
    * hillClimbingEquation.py
    * hillClimbingScheduling.py
<br>


--

# 上課內容 (Course Content)

# Memo
* 課本： [人工智慧/02-爬山演算法/A-爬山演算法簡介_陳鍾誠_GitLab](https://gitlab.com/ccckmit/course/-/wikis/陳鍾誠/書籍/人工智慧/02-爬山演算法/A-爬山演算法簡介)
<br>


# Hill Climbing Algorithm (爬山演算法)

* 是一種 **最簡單的優化** 算法
* 相較於神經網路的梯度下降法，爬山演算法會比它來得較有彈性，較簡單、威力強大，但速度不如梯度下降。
<br>

* 乘上一個負號，函數示圖會相反。
* 缺點： **較容易找到最高點，不容易找到最低點**。
  * 可能只能找到 **區域最佳解**，非全域最佳解
<br>

|![hill](img/hill1.jpg)|
|:---:|
|兩個低點，可能只會找到 **區域最低點**。|

<br>  

* 不斷地找最低點 or 最高點
* 爬山演算法，只要確保他**一直是往下走(找最低點)**，或**往上走(找最高點)**。
* 單變數函數(x, y)、雙變數函數(x, y, z)
* 多變數，方向多樣，網格線多，隨機挑幾個變數，多方向嘗試。
<br>

* **爬山演算法不是貪婪式**的，所以**不一定式往最斜的方向走**，但**梯度下降法(神經網路)會**。

* 
<br>



--

# Gradient Descent (梯度下降法)
* **沿著梯度方向往下走** 的方法
* 是一種 **Greedy Algorithm (貪婪演算法)**
  * 因為它每次都**朝著最斜的方向走**，**企圖得到最大的下降幅度**。
* 神經網路的 **反傳遞演算法**，也算種梯度下降法。
* 反傳遞演算法： 一種採用《**自動微分技術**》的梯度下降法！
* 差異： 只是在計算梯度時，改採用《**反傳遞的自動微分**》方式，而不是《**直接以數值方式計算梯度**》，會 **增加搜尋的速度**。
<br>


* 透過設計幾層 conv 或relu 或pool等，來提升模型的辨識率。 (當然電腦的算力也要足夠)
<br>

--


# 課上範例示範筆記
* 執行範例程式碼： [02-optimize]()
* 範例筆記： [11-deepLearning_w16/README.md]()
    * hillClimbing1.py
    * hillClimbing2.py
    * hillClimbing2r.py
    * hillClimbingNumber.py




## 課上答疑 (Q&A)


--


## Reference 




