# Week 07 - Computer Chess

> Date: 2020/ 04/ 16 (Thur.)

## 課堂大綱 (Outline)

* Back Propagation (反傳遞算法)
<br>

* 範例講解
  * 檔案路徑： [/python/05-chess](https://gitlab.com/ccckmit/ai2/-/tree/master/python/05-chess)
  * 範例檔案
    * 01-gomoku
      * gomoku.py
<br>


--

# 上課內容 (Course Content)

# Memo
* 課本： [人工智慧/05-電腦下棋/電腦下棋_陳鍾誠_GitLab](https://gitlab.com/ccckmit/course/-/wikis/陳鍾誠/書籍/人工智慧/05-電腦下棋)
* Slide: [用十分鐘瞭解 《電腦到底是怎麼下棋的》_陳鍾誠](https://www.slideshare.net/ccckmit/ss-59361780)
<br>



# Computer Chess (電腦下棋)
* AlphaGo圍棋程式
* 實作：五子棋程式
* Min-Max 對局搜尋法
* Alpha-Beta 修剪算法

<br>




--

# 課上範例示範筆記
* 執行範例程式碼： [05-chess]()
* 範例筆記： [05-chess/README.md]()
  * 01-gomoku
    * gomoku.py



## 課上答疑 (Q&A)



--


## Reference 




