# Week 04 - Back Propagation

> Date: 2020/ 04/ 02 (Thur.)

## 課堂大綱 (Outline)

* Back Propagation (反傳遞算法)
<br>

* 範例講解
  * 檔案路徑： [/python/03-neuralnet](https://gitlab.com/ccckmit/ai2/-/tree/master/python/03-neuralnet)
  * 範例檔案
    * 04-net
      * net.py
      * net1.py
      * net2.py
<br>


--

# 上課內容 (Course Content)

# Memo
* 課本： [人工智慧/03-神經網路/C-反傳遞算法_陳鍾誠_GitLab](https://gitlab.com/ccckmit/course/-/wikis/陳鍾誠/書籍/人工智慧/03-神經網路/C-反傳遞算法)
* 梯度下降法大部分利用 sigmoid()
* And, Or, Xor gate
<br>



# Back Propagation (反傳遞算法)
* 《**梯度下降法**》是《**深度學習神經網路**》背後的學習算法。
* 缺點： 當輸入變數很多時，**純粹靠《多次前向計算》的《梯度下降法》速度會過慢**。
* 因此我們需要使用《反傳遞算法》**更有效率的計算《梯度》**，省下大量的計算。
* 經由**偏微分的連鎖規則**，又可視為自動微分，可以**從結果的梯度推算出前面的梯度**。
* 先正向傳遞，再反傳遞。

<br>




--

# 課上範例示範筆記
* 執行範例程式碼： [03-neuralnet]()
* 範例筆記： [03-neuralnet/README.md]()
  * 04-net
      * net.py
      * net1.py
      * net2.py




## 課上答疑 (Q&A)



--


## Reference 




