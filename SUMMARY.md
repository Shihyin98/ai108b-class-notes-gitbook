# Summary

* [Introduction](README.md)
* [W01-Python](w01-Python/w01-Python.md)
* [W02-Hill Cilbing](w02-HillCilbing/w02-hillClimbing.md)
* [W03-Radient Descent](w03-RadientDescent/w03-radientDescent.md)
* [W04-Back Propagation](w04-BackPropagation/w04-backPropagation.md)
* [W05-Day off](w05-day-off/w05-DayOff.md)
* [W06-Graph Searchh](w06-GraphSearch/w06-graphSearch.md)
* [W07-Computer Chess](w07-ComputerChess/w07-computerChess.md)
* [W14-Markov Model](w14-MarkovModel/w14-markovModel.md)
* [W15-EM Algorithm](w15-EMAlgorithm/w15-EM-Algorithm.md)
* [W16-Deep Learning](w16-DeepLearning/w16-deepLearning.md)

