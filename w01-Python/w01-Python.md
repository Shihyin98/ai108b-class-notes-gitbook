# Week 01 - Python

> Date: 2020/ 03/ 05 (Thur.)

## 課堂大綱 (Outline)
* Python 環境安裝 
  * Python 3.7, Python 3.8
  * 

* Python 程式語言、人工智慧 之簡介
* Hill Climbing Algorithm (爬山演算法)
<br>

* 範例講解
  * 檔案路徑： [/python/00-basic](https://gitlab.com/ccckmit/ai2/-/tree/master/python/00-basic)
  * 範例檔案
    * 01-hello
      * [hello.py]()
      * [area.py]()
    * 02-control
      * [if.py]()
      * [for.py]()
      * [break.py]()
      * [continue.py]()
    * 03-function
      * [hello.py]()
      * [max.py]()
<br>

* 主要網站資源
  * FaceBook 上課直播： [陳鍾誠的課程社團](https://www.facebook.com/groups/ccccourse/)
  * [陳鍾誠的個人網站 misavo.com](https://misavo.com/blog/陳鍾誠/課程/人工智慧)
    * 課程教材： 人工智慧、科學計算(第七章後)
  * GitLab
    * 範例講解-程式碼： [ai2_ccckmit](https://gitlab.com/ccckmit/ai2)
      * 範例路徑： [/python](https://gitlab.com/ccckmit/ai2/-/tree/master/python)
    * [課程--人工智慧](https://gitlab.com/ccckmit/course/-/wikis/陳鍾誠/課程/人工智慧)
      * 課程教材： 人工智慧、科學計算(第七章後)
  * GitHub
    * Fork [ai108b](https://github.com/ccccourse/ai108b)
<br>


--


# 上課內容 (Course Content)

# Memo
* 環境安裝 (Install)
    * [VScode](https://code.visualstudio.com/)
      * 開發環境的工具 VScode ( or PyCharm)
      * [PyCharm](https://www.jetbrains.com/education/download/#section=pycharm-edu)
    * [Git](https://git-scm.com/downloads)
    * [Python](https://www.python.org/downloads/)
      * 選擇版本Python 3.x (64-bit)
<br>

* Python 基礎－程式語言
* Python優點：
&emsp; 1. 使用大量的函式庫
&emsp; 2. 部分函式庫於Python安裝時就順便安裝好的，例如：math(數學庫)、random(亂數庫)。
<br>

* 去年課程 使用 NodeJS、JavaScript，今年課程 開發語言、工具 Python、VScode。
* **程式語言**的選擇 **→ 社群 → 資源  → 領域**
* 編成所使用的語言選擇不只考慮於語法語句的使用，「社群」也為重要的因素，**有了社群討論平台，便有開源代碼、開放資源**，也奠定了研究領域的大方向。
* 各類語言
  * **Python：人工智慧、機器學習、深度學習、科學計算**
  * JavaScript：網頁、網站的設計
  * **Julia** (程式語言) New，**待觀望**，具有如Python好寫好讀、C執行的速度
<br>

* Terminal_Command：
  ```
  $　python　   # 不打檔名，進入python直譯器。
  $　import     # 引入函式庫
  $  quit()     # 跳出 (離開python直譯器
  ```
<br>

* 安裝好 python 之後，可以用下列方式找到**安裝路徑** (知道python安裝在哪
  ```
  >>>import os
  >>> import sys
  >>> os.path.dirname(sys.executable)
  ```
* 然後將該路徑**加入控制台的系統路徑中**，這樣就能在 vscode 中使用 python 指令了
<br>
  
  |![python-install-path](img/python-install-path.png)|
  | :---------: |
  |  自己的安裝路徑  |

<br>

* What is AI?
  * [人工智慧簡介_陳鍾誠個人網站](https://misavo.com/blog/陳鍾誠/書籍/人工智慧/01-人工智慧簡介/A-甚麼是人工智慧？)
  * 要機器做到人做的事情
  * 指紋、虹膜、人臉辨識
<br>

--


# 課上範例示範筆記
* 執行範例程式碼： [00-basic]()
* 範例筆記： [00-basic/README.md]()
  * 01-hello
    * hello.py
    * area.py
  * 02-control
    * if.py
    * for.py
    * break.py
    * continue.py
  * 03-function
    * hello.py
    * max.py


## 課上答疑 (Q&A)



--


## Reference 




