# Week 15 - Machine Learning

> Date: 2020/ 06/ 11 (Thur.)

## 課堂大綱 (Outline)
* 範例講解
  * [EM 演算法實作](https://gitlab.com/ccckmit/ai2/-/blob/master/python/10-machineLearning/em/em.md?fbclid=IwAR0wRffIAbFYCW6nNwZK0zh5VdZUpbKpzVT8npMVa3mvUzp8AlwyjmEWur0)
<br>


* EM Algorithm (最大期望算法)
* Pytorch 的 Deep Learning 應用
  * venv 虛擬環境
<br>


* 執行的範例程式
  * 檔案路徑： [/10-machineLearning]()
    * em/em.py


--

# 上課內容 (Course Content)

# Memo
* 繳交報告的撰寫規則
  * [繳交《作業與報告》注意事項](https://github.com/ccccourse/ccc109a/blob/master/00/md/submit.md?fbclid=IwAR0GKKl9WWv47zB_rfclzlTcRL3BHnlwNtayqE-aJ-dfxjzY9HkODF4bfHA)
  * [舉例：QEMU 跨平台虛擬機原理解析](https://github.com/ccccourse/ccc109a/blob/master/00/md/qemuStudy.md?fbclid=IwAR1jBCpKyOFP75ElUnd9PcIbyYhpcYJP9n82YTFabwpYoksf5ZvnFylX0Zs)
<br>



# EM Algorithm (最大期望算法)
* EM, Expectation-Maximization Algorithm
* 是一種「**最大概似估計**」，只是加入了「**隱變數**」的概念，這種「**最大概似估計**」企圖最大化下列算式中的 [\theta](https://latex.codecogs.com/gif.latex?\theta) 值。
  ```math
  L(X;\theta)=P(X|\theta)=\sum_z P(X,z|\theta)
  ```
* 使用兩個步驟：
  * 估計
  * 最大化
* [EM 演算法_定義_陳鍾誠]([em0.md](https://gitlab.com/ccckmit/ai2/-/blob/master/python/10-machineLearning/em/em0.md))
* [EM 演算法_實作_陳鍾誠](https://gitlab.com/ccckmit/ai2/-/blob/master/python/10-machineLearning/em/em.md?fbclid=IwAR0YdFu4XmNsXe8LdGopGv15Wd-WXvCLv3spDe7U7MOp9SY0Auee1tFBu-Q))
* 兩個不公正銅板，與公正鄭銅板進行比較。
* 先 (隨機) 設定好初始代，再經由 EM Algorithm，推算 **可能的下一輪機率**，輸出的結果再經由 EM Algorithm運算一次，不斷迭代，最後會 **收斂**。
<br>

* [Expectation–maximization algorithm_wiki](https://en.wikipedia.org/wiki/Expectation%E2%80%93maximization_algorithm?fbclid=IwAR0lpVhkFuXceBpxTokzY1C0TI_ZgPfsK1eq2iEpXvlFxEg4dO850QIFOXc)
* [最大期望算法_維基百科](https://zh.wikipedia.org/zh-tw/最大期望算法?fbclid=IwAR1ijyrvfJu7L27U3j3azHCkIf7Aijx0ZozMsuadxaY399HZGry9owHrixI)
<br>

* 計算過程：
  * E 步驟
    * 先算出　**序列機率值**
    * 有了 **機率值** 再算出 **權重**，接著就可以得出 **期望值(估計值)**。
    * 最後 **加總**。
  *  M 步驟
    * 計算新一代的 pA、pB
    * **不斷迭代**
    * 最後 **收斂**
* 範例： 兩個不公正銅板
<br>

--

#  Pytorch in Deep Learning
* venv 虛擬環境
  * [venv — Creation of virtual environments](https://docs.python.org/3/library/venv.html?fbclid=IwAR19zPQB3Wi-Q7Wgg1QaXZgSwL6DtQb6qD8nPl_i5skInUNGzAR0SVPFePI)
  * [Installing packages using pip and virtual environments](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/?fbclid=IwAR2-qrmtk0KOPTo0U2_ZPf3UFinRaUCRV0hHQPbdWnIwfSaZspciy0e0ix4)
  * 因為可能會引用太多模型，及安裝各式各樣的版本套件，而導致 Python環境相衝。
  * venv 虛擬環境，解決了上述問題。
<br>



# 課上範例示範


--


## Reference 




