# Week 06 - Graph Search

> Date: 2020/ 04/ 09 (Thur.)

## 課堂大綱 (Outline)

* Back Propagation (反傳遞算法)
<br>

* 範例講解
  * 檔案路徑： [/python/04-graphSearch](https://gitlab.com/ccckmit/ai2/-/tree/master/python/04-graphSearch)
  * 範例檔案
    * 01-search
      * graph_search.py
<br>


--

# 上課內容 (Course Content)

# Memo
* 課本： [人工智慧/03-神經網路/C-反傳遞算法_陳鍾誠_GitLab](https://gitlab.com/ccckmit/course/-/wikis/陳鍾誠/書籍/人工智慧/03-神經網路/C-反傳遞算法)
* 深度學習套件: tensorflow, pytorch
<br>

* 實例應用
  * 老鼠走迷宮： 深度
    * [實作：老鼠走迷宮問題_陳鍾誠_GitLab](https://gitlab.com/ccckmit/course/-/wikis/陳鍾誠/書籍/人工智慧/04-圖形搜尋/C-實作：老鼠走迷宮問題)
  * 拼圖問題：深度 或 廣度
    * [實作：拼圖問題_陳鍾誠_GitLab](https://gitlab.com/ccckmit/course/-/wikis/陳鍾誠/書籍/人工智慧/04-圖形搜尋/D 實作：拼圖問題)
  * 過河的問題--狼、羊、甘藍菜
    * [實作：《狼、羊、甘藍菜》過河問題_陳鍾誠_GitLab](https://gitlab.com/ccckmit/course/-/wikis/陳鍾誠/書籍/人工智慧/04-圖形搜尋/E-實作：《狼、羊、甘藍菜》過河問題)
<br>



# Graph Search (圖形搜尋)
* 圖形搜尋的方法，分 3類：
  * DFS (Depth-First Search, 深度優先搜尋)
  * BFS (Breath-First Search, 廣度優先搜尋)
  * BestFS (Best-First Search, 最佳優先搜尋)
* A* 搜尋法
<br>

* 深度優先搜尋
  * 遞迴技巧

* 廣度優先搜尋
  * 必須採用 **FIFO** (First-in First-Out, 先進先出) 的方式管理節點
  * 因此通常會有個 **佇列 (queue)** 結構
<br>

* 最佳優先搜尋
  * 缺點：
    * 深度搜尋 會無頭緒的單方向的片面搜索，不夠全面，沒有效率。
    * 廣度搜尋 **耗費太多的記憶體**，無法很快的找到目標點，沒有效率。
<br>


--

# 課上範例示範筆記
* 執行範例程式碼： [04-graphSearch]()
* 範例筆記： [04-graphSearch/README.md]()
  * 01-search
    * graph_search.py


## 課上答疑 (Q&A)



--


## Reference 




